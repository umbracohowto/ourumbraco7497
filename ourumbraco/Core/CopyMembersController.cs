﻿using System.Linq;
using System.Threading;
using Umbraco.Web.Mvc;

namespace UmbracoV8.Core
{
	public class CopyMembersController : SurfaceController
	{
		private static readonly string targetMemberContentTypeAlias = "Member";
		private static readonly string sourceMemberContentTypeAlias = "specialMember";
		private static readonly string processedMemberPrefix = $"{targetMemberContentTypeAlias} # ";

		public void Index(string secret)
		{
			if (secret == "ea8a29a858404b9490c95dd5bd119fc5" && !string.IsNullOrWhiteSpace(targetMemberContentTypeAlias) && !string.IsNullOrWhiteSpace(sourceMemberContentTypeAlias) && targetMemberContentTypeAlias != sourceMemberContentTypeAlias)
			{
				var targetMembers = Services.MemberService.GetMembersByMemberType(targetMemberContentTypeAlias).Where(m => !m.Username.StartsWith(processedMemberPrefix));
				foreach (var member in targetMembers)
				{
					Write($"<p>Processing {member.Id} with Username '{member.Username}' of Alias {member.ContentTypeAlias}<br>");
					var newMember = Services.MemberService.CreateMemberWithIdentity(member.Username, member.Email, member.Name, sourceMemberContentTypeAlias);
					Write($"Created copy of {member.Id} New Id is {newMember.Id} with Username '{newMember.Username}' with Alias {newMember.ContentTypeAlias}<br>");
					member.Name = $"{newMember.Id}";
					member.Username = $"{processedMemberPrefix}{member.Username}";
					Services.MemberService.Save(member);
					Write($"Saving processed Member with Id {member.Id} with updated Username '{member.Username}' & updated Name '{member.Name}' of Alias {member.ContentTypeAlias} this  can be deleted later after you use it to verify the member password on their first login<br>");
					//Services.MemberService.Delete(member);
					Thread.Sleep(500);
				}
				Write($"<p>DONE<p>");
				var targetMembersToDelete = Services.MemberService.GetMembersByMemberType(targetMemberContentTypeAlias).Where(m => m.Username.StartsWith(processedMemberPrefix));
			}
		}

		private void Write(string msg)
		{
			Response.Output.WriteLine(msg);
			Response.Output.Flush();
			Response.Flush();
		}
	}
}